#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#3a1e28', '#3a1e28']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#941e29', '#941e29']
    brightred       = ['#d84a4c', '#d84a4c']
    green           = ['#686d71', '#686d71']
    brightgreen     = ['#4f7b7f', '#4f7b7f']
    blue            = ['#776e8f', '#776e8f']
    darkblue        = ['#4cb1ff', '#4cb1ff']
    magenta         = ['#94536d', '#94536d']
    brightmagenta   = ['#f5c1ce', '#f5c1ce']
    cyan            = ['#d84a4c', '#d84a4c']
    brightcyan      = ['#e79049', '#e79049']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Flannery/BG.png"
