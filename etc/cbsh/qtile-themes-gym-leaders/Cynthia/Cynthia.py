#!/usr/bin/env python3
class Colors(object):

    black           = ['#444546', '#444546']
    grey            = ['#aeb1b3', '#aeb1b3']
    white           = ['#ffffff', '#ffffff']
    red             = ['#68617d', '#68617d']
    brightred       = ['#7f85a1', '#7f85a1']
    green           = ['#f27ea3', '#f27ea3']
    brightgreen     = ['#e83925', '#e83925']
    blue            = ['#4b5171', '#4b5171']
    darkblue        = ['#3a3d59', '#3a3d59']
    magenta         = ['#94536d', '#94536d']
    brightmagenta   = ['#f5c1ce', '#f5c1ce']
    cyan            = ['#e5e2a3', '#e5e2a3']
    brightcyan      = ['#fae3a8', '#fae3a8']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"


class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Cynthia/BG.png"
